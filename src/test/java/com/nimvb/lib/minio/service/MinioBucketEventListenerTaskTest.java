package com.nimvb.lib.minio.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.nimvb.lib.minio.ApplicationTestConfiguration;
import io.minio.MinioClient;
import lombok.RequiredArgsConstructor;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ApplicationTestConfiguration.class})
class MinioBucketEventListenerTaskTest {

    @Autowired private MinioClient client;
    @Autowired private AmazonS3 s3Client;
    @Autowired @Qualifier("minioBucketEventListenerThreadPool") private ThreadPoolTaskExecutor taskExecutor;
    @Autowired private ThreadPoolTaskExecutor primaryTaskExecutor;

    @Test
    void test() throws InterruptedException {
        Assertions.assertThat(client).isNotNull();
        Assertions.assertThat(s3Client).isNotNull();
        Assertions.assertThat(taskExecutor).isNotNull();
        Assertions.assertThat(taskExecutor.getCorePoolSize()).isEqualTo(4);
        Assertions.assertThat(primaryTaskExecutor.getCorePoolSize()).isEqualTo(1);
    }

}