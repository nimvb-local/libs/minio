package com.nimvb.lib.minio.context;

import com.nimvb.lib.minio.annotation.MinioBucketEventListener;
import io.minio.messages.NotificationRecords;
import org.springframework.stereotype.Component;

@Component
public class BucketEventListener {

    @MinioBucketEventListener(bucket = "nimvb",events = {"s3:ObjectCreated:Put"},prefix = "",suffix = "")
    void listen(NotificationRecords records){
        System.out.println("Notification received from " + Thread.currentThread().getId());
    }


    @MinioBucketEventListener(bucket = "nimvb",events = {"s3:ObjectCreated:Put"},prefix = "",suffix = "")
    void listen1(NotificationRecords records){
        System.out.println("Notification received from " + Thread.currentThread().getId());
    }
}
