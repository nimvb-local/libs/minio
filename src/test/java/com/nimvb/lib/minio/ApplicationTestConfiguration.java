package com.nimvb.lib.minio;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@EnableAutoConfiguration
@SpringBootConfiguration
public class ApplicationTestConfiguration {

    @Primary
    @Bean
    ThreadPoolTaskExecutor threadPoolTaskExecutor(){
        return new ThreadPoolTaskExecutor();
    }
}
