package com.nimvb.lib.minio.config;

import com.nimvb.lib.minio.annotation.MinioBucketEventListener;
import com.nimvb.lib.minio.service.MinioBucketEventListenerTask;
import io.minio.MinioClient;
import io.minio.messages.NotificationRecords;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.lang.reflect.Method;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;

@Configuration
@AutoConfigureAfter(MinioThreadPoolConfiguration.class)
public class MinioBucketEventListenersConfiguration implements ApplicationContextAware {

    private final MinioClient client;
    private final ThreadPoolTaskExecutor executor;
    private final ConcurrentLinkedQueue<Future<Void>> listeners = new ConcurrentLinkedQueue<>();

    public MinioBucketEventListenersConfiguration(MinioClient minioClient, @Qualifier(value = "minioBucketEventListenerThreadPool") ThreadPoolTaskExecutor executor) {
        this.client = minioClient;
        this.executor = executor;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        for (String beanName : applicationContext.getBeanDefinitionNames()) {
            Object bean = applicationContext.getBean(beanName);
            Class<?> beanClass = bean.getClass();
            if (AopUtils.isAopProxy(bean)) {
                beanClass = AopUtils.getTargetClass(bean);
            }

            for (Method method : beanClass.getDeclaredMethods()) {
                if (method.isAnnotationPresent(MinioBucketEventListener.class)) {
                    if (method.getParameterCount() != 1) {
                        throw new IllegalArgumentException("Bucket event listener should have only one parameter type of NotificationRecords");
                    }

                    if (method.getParameterTypes()[0] != NotificationRecords.class) {
                        throw new IllegalArgumentException("Parameter type should be NotificationRecords");
                    }

                    MinioBucketEventListener bucketEventListener = method.getAnnotation(MinioBucketEventListener.class);

                    final Future<Void> submit = executor.submit(new MinioBucketEventListenerTask(bean, method, client, bucketEventListener));
                    listeners.add(submit);

                }
            }
        }

    }
}
