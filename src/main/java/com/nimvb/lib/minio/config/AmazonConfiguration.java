package com.nimvb.lib.minio.config;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.nimvb.lib.minio.properties.MinioProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(AmazonS3.class)
@AutoConfigureAfter(MinioConfiguration.class)
public class AmazonConfiguration {

    @ConditionalOnMissingBean
    @Bean
    AmazonS3 amazonS3(MinioProperties properties){
        return AmazonS3ClientBuilder
                .standard()
                .withPathStyleAccessEnabled(true)
                .withEndpointConfiguration(endpointConfiguration(properties))
                .withCredentials(credentialConfiguration(properties))
                .build();
    }

    private AWSCredentialsProvider credentialConfiguration(MinioProperties properties) {
        return new AWSStaticCredentialsProvider(new BasicAWSCredentials(properties.getAccessKey(), properties.getSecretKey()));
    }

    private AwsClientBuilder.EndpointConfiguration endpointConfiguration(MinioProperties properties) {
        return new AwsClientBuilder.EndpointConfiguration(properties.getUrl(), Regions.EU_WEST_1.getName());
    }


}
