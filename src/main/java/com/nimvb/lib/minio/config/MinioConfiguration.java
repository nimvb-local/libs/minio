package com.nimvb.lib.minio.config;

import com.google.common.net.HttpHeaders;
import com.nimvb.lib.minio.properties.MinioProperties;
import io.minio.MinioClient;
import lombok.RequiredArgsConstructor;
import okhttp3.*;
import okhttp3.Authenticator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Nullable;
import java.io.IOException;
import java.net.*;

@Configuration
@ConditionalOnClass(MinioClient.class)
@EnableConfigurationProperties(MinioProperties.class)
@ComponentScan("com.nimvb.lib.minio")
@RequiredArgsConstructor
public class MinioConfiguration {


    @ConditionalOnMissingBean
    @Bean
    public MinioClient minioClient(MinioProperties properties) {
        MinioClient client;
        MinioClient.Builder builder = MinioClient.builder();
        builder
                .endpoint(properties.getUrl())
                .credentials(properties.getAccessKey(), properties.getSecretKey());
        if (properties.getProxy().isEnabled()) {
            builder.httpClient(httpClient(properties));
        }
        client = builder.build();
        long connectionTimeout = properties.getTimeoutConfiguration().getConnectionTimeout();
        long readTimeout = properties.getTimeoutConfiguration().getReadTimeout();
        long writeTimeout = properties.getTimeoutConfiguration().getWriteTimeout();
        client.setTimeout(connectionTimeout, readTimeout, writeTimeout);
        return client;
    }

    private OkHttpClient httpClient(MinioProperties properties) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        Proxy.Type proxyType = MinioProperties.ProxyType.toJavaType(properties.getProxy().getType());
        String host = properties.getProxy().getSettings().getHost();
        Integer port = properties.getProxy().getSettings().getPort();
        InetSocketAddress proxyAddress = new InetSocketAddress(host, port);
        Proxy proxy = new Proxy(proxyType, proxyAddress);
        if (properties.getProxy().getSettings().isAuthenticationEnabled()) {
            String username = properties.getProxy().getSettings().getCredentials().getUsername();
            String password = properties.getProxy().getSettings().getCredentials().getPassword();
            if (proxyType == Proxy.Type.HTTP) {
                builder.proxyAuthenticator(httpAuthenticator(username, password));
            } else if (proxyType == Proxy.Type.SOCKS) {
                socksAuthenticator(host, port, username, password);
            }
        }
        builder.proxy(proxy);
        return builder.build();
    }

    private void socksAuthenticator(String host, int port, String username, String password) {
        java.net.Authenticator authenticator = new java.net.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                if (getRequestingHost().equalsIgnoreCase(host)) {
                    if (getRequestingPort() == port) {
                        return new PasswordAuthentication(username, password.toCharArray());
                    }
                }
                return super.getPasswordAuthentication();
            }
        };

        java.net.Authenticator.setDefault(authenticator);
    }

    private Authenticator httpAuthenticator(String username, String password) {
        okhttp3.Authenticator authenticator = new Authenticator() {
            @Nullable
            @Override
            public Request authenticate(@Nullable Route route, Response response) throws IOException {
                String credential = Credentials.basic(username, password);
                return response.request().newBuilder().header(HttpHeaders.PROXY_AUTHORIZATION, credential).build();
            }
        };
        return authenticator;
    }

}
