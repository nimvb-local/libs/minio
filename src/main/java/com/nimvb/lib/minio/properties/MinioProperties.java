package com.nimvb.lib.minio.properties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.net.Proxy;


@ConfigurationProperties("spring.minio")
@Data
public class MinioProperties {
    private String url = "https://play.min.io";
    private String accessKey = "Q3AM3UQ867SPQQA43P2F";
    private String secretKey = "zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG";
    private ProxyConfiguration proxy = new ProxyConfiguration();
    private TimeoutConfiguration timeoutConfiguration = new TimeoutConfiguration();

    @Data
    public static class TimeoutConfiguration {
        private long connectionTimeout = 300000L;
        private long readTimeout = 300000L;
        private long writeTimeout = 300000L;
    }

    @Data
    public static class ProxyConfiguration {
        private ProxyType type = ProxyType.HTTP;
        private boolean enabled = false;
        private ProxySettings settings = new ProxySettings();
    }

    @Data
    public static class ProxySettings {
        public String host = "";
        public Integer port = -1;
        public boolean authenticationEnabled = false;
        public Credentials credentials = new Credentials();

        @Data
        public static class Credentials {
            private String username = "";
            private String password = "";
        }
    }

    @AllArgsConstructor
    @Getter
    public enum ProxyType {
        HTTP("http"),
        SOCKS("socks");
        private String value;

        public static Proxy.Type toJavaType(ProxyType type) {
            switch (type) {

                case HTTP:
                    return Proxy.Type.HTTP;
                case SOCKS:
                    return Proxy.Type.SOCKS;
            }

            throw new RuntimeException();
        }
    }
}

