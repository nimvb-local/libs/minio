package com.nimvb.lib.minio.service;

import com.nimvb.lib.minio.annotation.MinioBucketEventListener;
import io.minio.CloseableIterator;
import io.minio.ListenBucketNotificationArgs;
import io.minio.MinioClient;
import io.minio.Result;
import io.minio.errors.*;
import io.minio.messages.NotificationRecords;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.lang.reflect.Method;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Callable;

@RequiredArgsConstructor
public class MinioBucketEventListenerTask implements Callable<Void> {
    private final Object instance;
    private final Method method;
    private final MinioClient client;
    private final MinioBucketEventListener listener;
    @Override
    public Void call() throws Exception {
        while (true){
            try{
                ListenBucketNotificationArgs args = ListenBucketNotificationArgs.builder().bucket(listener.bucket()).prefix(listener.prefix()).suffix(listener.suffix()).events(listener.events()).build();
                try(CloseableIterator<Result<NotificationRecords>> iterator = client.listenBucketNotification(args)) {
                    while (iterator.hasNext()){
                        final Result<NotificationRecords> next = iterator.next();
                        final NotificationRecords notificationRecords = next.get();
                        boolean accessible = method.canAccess(instance);
                        if(!accessible){
                            method.trySetAccessible();
                        }
                        method.invoke(instance,notificationRecords);
                        method.setAccessible(accessible);
                    }

                }

            }catch (Exception ex){
                throw ex;
            }
        }
    }
}
