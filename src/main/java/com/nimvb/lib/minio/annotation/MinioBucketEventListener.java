package com.nimvb.lib.minio.annotation;

import io.minio.ListenBucketNotificationArgs;
import org.springframework.scheduling.annotation.Async;

import java.lang.annotation.*;

@Async
@Target(ElementType.METHOD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface MinioBucketEventListener {

    String[] events();
    String bucket() default  "";
    String prefix() default "";
    String suffix() default  "";
}
